import { useState, useCallback } from 'react';

const useHttp = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const sendRequest = useCallback(async (applyData, reqUrl, reqParams = null) => {
        setIsLoading(true);
        setError(null);
        try {
            const response = await fetch(reqUrl, reqParams);
            if (!response.ok) {
                throw new Error('Request failed!');
            }
            const responseData = await response.json();
            applyData(responseData);
        } catch (err) {
            setError(err.message || 'Something went wrong!');
        }
        setIsLoading(false);
    }, []);

    return { isLoading, error, sendRequest };
}

export default useHttp;