import Section from '../UI/Section';
import TaskForm from './TaskForm';
import useHttp from '../../hooks/use-http';

const NewTask = (props) => {
  const { isLoading, error, sendRequest: createTaskRequest } = useHttp();

  // const transformTask = (taskText, responseData) => {
  //   const generatedId = responseData.name;
  //   const createdTask = { id: generatedId, text: taskText };
  //   props.onAddTask(createdTask);
  // }

  const enterTaskHandler = (taskText) => {
    const transformTask = (responseData) => {
      const generatedId = responseData.name;
      const createdTask = { id: generatedId, text: taskText };
      props.onAddTask(createdTask);
    }

    createTaskRequest(
      transformTask,
      // transformTask.bind(null, taskText),
      'https://react-movies-d0726-default-rtdb.europe-west1.firebasedatabase.app/tasks.json', {
      method: 'POST',
      body: JSON.stringify({ text: taskText }),
      headers: {
        'Content-Type': 'application/json',
      }
    });
  };

  return (
    <Section>
      <TaskForm onEnterTask={enterTaskHandler} loading={isLoading} />
      {error && <p>{error}</p>}
    </Section>
  );
};

export default NewTask;
