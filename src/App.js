import React, { useEffect, useState } from 'react';

import Tasks from './components/Tasks/Tasks';
import NewTask from './components/NewTask/NewTask';
import useHttp from './hooks/use-http';

function App() {
  const [tasks, setTasks] = useState([]);

  const { isLoading, error, sendRequest: fetchTasksRequest } = useHttp();

  useEffect(() => {
    const transformTasks = (tasks) => {
      const loadedTasks = [];
      for (const taskKey in tasks) {
        loadedTasks.push({ id: taskKey, text: tasks[taskKey].text });
      }
      setTasks(loadedTasks);
    };

    fetchTasksRequest(transformTasks, 'https://react-movies-d0726-default-rtdb.europe-west1.firebasedatabase.app/tasks.json');
  }, [fetchTasksRequest]);

  const taskAddHandler = (task) => {
    setTasks((prevTasks) => prevTasks.concat(task));
  };

  return (
    <React.Fragment>
      <NewTask onAddTask={taskAddHandler} />
      <Tasks
        items={tasks}
        loading={isLoading}
        error={error}
        onFetch={fetchTasksRequest}
      />
    </React.Fragment>
  );
}

export default App;
